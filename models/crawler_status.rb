class CrawlerStatus < ActiveRecord::Base
  self.primary_key = 'created_at'
  scope :last_status, order('process_timestamp DESC')
  scope :starteds, where(action: 'Starting', status: 'Wait')
  attr_accessible :process_timestamp, :action, :status

  def self.running_now?
    last_status = CrawlerStatus.last_status.first
    return false unless last_status
    last_status.action == 'Starting' && last_status.status == 'Wait'
  end

  def self.start_crawler
    return true if self.running_now?
    self.create!(process_timestamp: Time.now, action: 'Starting', status: 'Wait')
  end

  def self.end_crawler
    self.create!(process_timestamp: Time.now, action: 'Finished', status: 'Closed') if self.running_now?
  end

end

# id  runtime downloadtime  starttime closetime productscount categorycount sitecount created_at  updated_at  content solr_indexed