class WorkitBplList < ActiveRecord::Base
  belongs_to :workit_product_family
  belongs_to :category
  belongs_to :base_product
  attr_accessible :workit_product_family_id, :category_id, :base_product_id, :brand, :model, :name, :icrt_code

  def self.rebuild
    # def rebuild_data
      begin
        ActiveRecord::Base.connection.execute('TRUNCATE TABLE workit_bpl_lists')
      rescue
        ActiveRecord::Base.connection.execute('DELETE FROM workit_bpl_lists')
      end
      
      ActiveRecord::Base.connection.execute("insert into workit_bpl_lists 
      ( category_id, workit_product_family_id, base_product_id, brand, model, name, icrt, created_at, updated_at ) 
      ( select wpf.category_id, wpf.id, ibp.base_product_id, ibp.brand, ibp.model, ibp.name, ibp.icrt, getdate(), getdate() 
      from workit_product_families wpf, input_base_products ibp 
      where wpf.category_id = ibp.category_id and ibp.country = wpf.country)")
    # end
  end

  def self.by_country(country)
    WorkitBplList.joins(:workit_product_family).where(workit_product_families: {country: country}).collect do |workit_bpl_list|
      {
        family: workit_bpl_list.workit_product_family.category.id,
        family_name: workit_bpl_list.workit_product_family.category.name, 
        country: workit_bpl_list.workit_product_family.country, 
        name: workit_bpl_list.name, 
        model: workit_bpl_list.model, 
        brand: workit_bpl_list.brand, 
        encodex_average_price: 0.0, 
        icrt: workit_bpl_list.icrt, 
        sku: workit_bpl_list.base_product_id
      }
    end
  end
end