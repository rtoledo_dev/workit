require 'uri'
module WorkitCrawler
  class MigrationToOlp
    
    def initialize
      @workit_product_prices = WorkitProductPrice.waiting_processing.all
    end

    def migrate_all
      WorkitCrawler.puts_action("Migration to OLP database started at #{Time.now}")
      update_all_to_inactive

      @workit_product_prices.each do |workit_product_price|
        begin
          migrate_product(workit_product_price)
          workit_product_price.processed!
        rescue => e
          WorkitCrawler.puts_action("Error to migrate workit_product_price id #{workit_product_price.id}")
          WorkitCrawler.puts_action(e.message)
          # workit_product_price.processed_with_error(e.message)
        end
      end
      WorkitCrawler.puts_action("Migration to OLP database ended at #{Time.now}")
    end

    protected

    def update_all_to_inactive
      ActiveRecord::Base.connection.execute("update all_products set active = 0 where site_id in ( select id from sites where source = 'workit')")
      # colocar category id tb
      # 182962
      ActiveRecord::Base.connection.execute("DELETE FROM products WHERE site_id IN ( select id from sites where source = 'workit')")
    end

    

    def migrate_product(workit_product_price)
      site          = build_site(workit_product_price)
      site_category = build_site_category(workit_product_price, site)
      all_product   = build_all_product(workit_product_price, site, site_category)
      return if all_product.in_stock.to_i == 0 || all_product.price == "&euro; 0.00"
      product       = build_product(all_product)
      build_basic_product_base_product(workit_product_price,product)
    end

    def build_site(workit_product_price)
      site_id         = workit_product_price.site_id
      site_name       = workit_product_price.site_name
      site_url        = URI.parse(workit_product_price.url.to_s.gsub(' ','-'))
      site_url        = "#{site_url.scheme}://#{site_url.host}/"
      site_attributes = {
        name:           site_url,
        css_selector:   nil,
        country:        'italy', #workit_product_price.country
        disabled:       0,
        null_urls:      0,
        pretty_name:    site_name,
        source:         'workit',
        products_count: 0,
        unique_id:      nil,
        evaluated:      nil,
        excluded:       0
      }
      
      # site = Site.where(["(pretty_name = ? OR id = ?) AND country = ? AND source = ?",site_name,site_id, workit_product_price.country, 'workit']).first
      site = Site.where(["(pretty_name = ? OR id = ?) AND source = ?",site_name,site_id,'workit']).first
      unless site
        site = Site.new(site_attributes)
        site.save!
      end
      site
    end

    def build_site_category(workit_product_price, site)
      category_id   = workit_product_price.category_id
      category      = Category.find(category_id)
      site_category = site.site_categories.where(category_id: category.id).first
      unless site_category
        site_category = site.site_categories.build(category_id: category.id, status_crawler: 1, status_publish_to_ps: 1, sub_category_name: category.name)
        site_category.save!
      end
      site_category
    end

    def build_all_product(workit_product_price, site, site_category)
      unique_id       = AllProduct.new(name: workit_product_price.title, url: workit_product_price.url, site_category_id: site_category.id, site_id: site.id).generate_unique_id
      all_product     = AllProduct.where(unique_id: unique_id).first

      all_product_attributes = {
        active:     true,
        last_found: Date.today,
        name:               workit_product_price.title,
        price:              "&euro; #{workit_product_price.price.round(2)}",
        url:                workit_product_price.url,
        site_id:            site.id,
        site_category_id:   site_category.id,
        in_stock:           workit_product_price.availability,
        raw_price:          "&euro; #{workit_product_price.price.round(2)}",
        raw_name:           workit_product_price.title
      }
      unless all_product
        all_product_attributes = all_product_attributes.merge({
          country:            'italy', #workit_product_price.country
          trovaprezzi:        0,
          unique_id:          unique_id,
        })
      end

      all_product ||= AllProduct.new
      all_product.attributes = all_product_attributes
      all_product.save!
      all_product
    end

    def build_product(all_product)
      product = Product.where(unique_id: all_product.unique_id).first

      product_attributes = {
        active:     true,
        last_found: Date.today,
        name:               all_product.name,
        price:              all_product.price,
        url:                all_product.url,
        site_id:            all_product.site_id,
        site_category_id:   all_product.site_category_id,
        in_stock:           all_product.in_stock,
        raw_price:          all_product.raw_price,
        raw_name:           all_product.raw_name
      }
      unless product
        product_attributes = product_attributes.merge({
          country:            all_product.country,
          trovaprezzi:        all_product.trovaprezzi,
          unique_id:          all_product.unique_id,
        })
      end

      product ||= Product.new
      product.attributes = product_attributes
      product.save!
      product
    end

    def build_basic_product_base_product(workit_product_price, product)
      base_product = BaseProduct.find(workit_product_price.base_product_id)
      basic_product_base_product = BasicProductBaseProduct.where(product_unique_id: product.unique_id, base_product_unique_id: base_product.unique_id).first
      unless basic_product_base_product
        BasicProductBaseProduct.new(product_unique_id: product.unique_id, base_product_unique_id: base_product.unique_id, match_type: 'workit').save!
      else
        basic_product_base_product.update_attribute(:match_type, 'workit')
        basic_product_base_product.touch
      end

      product
    end
  end
end