require 'net/ftp'
module WorkitCrawler
  class WorkitLoad
    
    FTP_GET_FILES_ADDRESS         = 'out.w2p.workit.fr'
    FTP_USER                      = 'euroconsumer'
    FTP_PASSWORD                  = 'w6xi6VD3'
    PATH_TO_WORKIT_FILES          = File.join('tmp','workit_files')
    PATH_TO_IMPORTED_WORKIT_FILES = File.join(PATH_TO_WORKIT_FILES,'imported')

    def initialize
      
    end
    
    def save_files_on_database
      WorkitCrawler.puts_action("Download and pre-import processes started at #{Time.now}")
      WorkitLoad.truncate_workit_product_prices
      if files_to_process.blank?
        WorkitCrawler.puts_action("no files to be imported")
      else
        files_to_process.each do |file_path|
          save_file(file_path)
        end
        WorkitCrawler.puts_action("Imported #{WorkitProductPrice.count} products from #{files_to_process.size} files")
      end
      WorkitCrawler.puts_action("Download and pre-import processes ended at #{Time.now}")
    end

    def self.truncate_workit_product_prices
      begin
        ActiveRecord::Base.connection.execute('TRUNCATE TABLE workit_product_prices')
      rescue
        ActiveRecord::Base.connection.execute('DELETE FROM workit_product_prices')
      end
    end

    def save_file(file_path)
      WorkitCrawler.puts_action("saving: #{File.basename(file_path)}")
      file = File.open(file_path)
      doc  = Nokogiri::XML(file)
      doc.xpath('//CATALOGPRODUCT').each do |catalog_product_node|
        workit_product_price_attributes                   = {}
        workit_product_price_attributes[:brand]           = catalog_product_node.at('BRAND').text
        workit_product_price_attributes[:base_product_id] = catalog_product_node.at('SKU').text
        workit_product_price_attributes[:category_id]     = catalog_product_node.at('CATEGORY_ID').text
        catalog_product_node.xpath('.//PRODUCT').each do |product_node|
          
          begin
            workit_bpl_list = WorkitBplList.joins(:workit_product_family)
            .where(workit_product_families: {country: product_node.at('COUNTRY').text, category_id: workit_product_price_attributes[:category_id]}, 
                   base_product_id: workit_product_price_attributes[:base_product_id]).first
            raise 'bpl not found' unless workit_bpl_list
            workit_product_price_attributes[:workit_bpl_list_id] = workit_bpl_list.id
            workit_product_price_attributes[:title]              = product_node.at('TITLE').text
            workit_product_price_attributes[:site_name]          = product_node.at('SITE').text
            workit_product_price_attributes[:site_id]            = product_node.at('SITE_ID').text
            workit_product_price_attributes[:url]                = product_node.at('URL').text
            workit_product_price_attributes[:price]              = product_node.at('PRICE').text.to_f
            workit_product_price_attributes[:delivery_price]     = product_node.at('DELIVERYPRICE').text.to_f
            workit_product_price_attributes[:variation]          = product_node.at('VARIATION').text.to_f
            workit_product_price_attributes[:delivery]           = product_node.at('DELIVERY').text.to_f
            workit_product_price_attributes[:availability]       = product_node.at('AVAILABILITY').text.downcase == 'true'
            workit_product_price_attributes[:image_url]          = product_node.at('IMAGE').text
            workit_product_price_attributes[:modified_at]        = WorkitLoad.convert_datetime(product_node.at('MODIFIED').text)
          
            workit_product_price = WorkitProductPrice.new(workit_product_price_attributes)
            workit_product_price.save!
          rescue => e
            WorkitProductPrice.save_with_error_on_importing(e.message,workit_product_price_attributes)
          end
        end
      end
      file.close
      FileUtils.mv(file_path,PATH_TO_IMPORTED_WORKIT_FILES)
    end

    def self.convert_datetime(modified_at)
      modified_at_parts      = modified_at.split(' ')
      modified_at_time       = modified_at_parts.first
      modified_at_date_parts = modified_at_parts.first.split('/')
      modified_at_year       = modified_at_date_parts.last.to_i
      modified_at_year       = modified_at_year <= 99 ? (2000+modified_at_year) : modified_at_year
      modified_at_date       = "#{modified_at_year}-#{modified_at_date_parts[1]}-#{modified_at_date_parts[0]}"
      "#{modified_at_date} #{modified_at_time}"
    end


    def files_to_process
      return @files unless @files.nil?
      WorkitLoad.create_workit_directories
      sync_ftp_files
      @files = local_files
    end


    def local_files
      xml_pattern  = File.join(PATH_TO_WORKIT_FILES,'*.xml')
      Dir.glob(xml_pattern) || []
    end

    def sync_ftp_files
      begin
        ftp         = Net::FTP.new(FTP_GET_FILES_ADDRESS, FTP_USER, FTP_PASSWORD)
        ftp.passive = true
        files = ftp.nlst("*#{Date.today.strftime('%d%m%Y')}.xml")
        # ftp.chdir('TEST')
        # files = ftp.nlst("*#{Date.today.strftime('%m%Y')}.xml")
        WorkitCrawler.puts_action("Connected on ftp")
        files.each do |file|
          WorkitCrawler.puts_action("Downloading ftp file #{file}")
          ftp.gettextfile(file, File.join(PATH_TO_WORKIT_FILES,file))
        end
      rescue => e
        WorkitCrawler.puts_action("error on ftp")
        WorkitCrawler.puts_action(e.message)
      end
    end

    def self.create_workit_directories
      FileUtils.mkdir_p(PATH_TO_WORKIT_FILES, mode: 0777) unless Dir.exists?(PATH_TO_WORKIT_FILES)
      FileUtils.mkdir_p(PATH_TO_IMPORTED_WORKIT_FILES, mode: 0777) unless Dir.exists?(PATH_TO_IMPORTED_WORKIT_FILES)
    end
  end
end