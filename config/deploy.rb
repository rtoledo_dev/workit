require "rvm/capistrano"
require 'bundler/capistrano'

default_run_options[:pty] = true
set :application, "olpws"
set :rvm_ruby_string, "1.9.3@#{application}"
set :rvm_type, :user

set :user, "railsapps"
set :use_sudo, false
set :ssh_options, { :forward_agent => true }

set :scm, :git
set :repository, "git@bitbucket.org/rtoledo/#{application}.git"
set :branch, "master"
set :deploy_via, :copy
set :deploy_to, "/home/#{user}/#{application}"

set :bundle_flags,    nil
set :bundle_without,  nil

set :unicorn_port, 9191
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid, "#{deploy_to}/shared/pids/unicorn.pid"

after "deploy", "deploy:cleanup", "rvm:trust_rvmrc"

task :development do
  set :app_env, 'development'
  set :branch, "develop"
  server "10.7.1.113", :app, :web, :db, :primary => true
end

task :approval do
  select_release
  set :app_env, 'approval'
  server "10.7.1.190", :app, :web, :db, :primary => true
end

task :approval_belgium do
  select_release
  set :app_env, 'approval_belgium'
  server "bel-aums.conseur.org", :app, :web, :db, :primary => true
end

task :production do
  select_release
  set :app_env, 'production'
  server "bel-olp.conseur.org", :app, :web, :db, :primary => true
end

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end

namespace :deploy do
  task :restart do
    run "if [ -f #{unicorn_pid} ]; then kill -USR2 `cat #{unicorn_pid}`; else cd #{deploy_to}/current && unicorn -c #{unicorn_conf} -E #{app_env} -p #{unicorn_port} -D; fi"
  end
  task :start do
    run "cd #{deploy_to}/current && unicorn -c #{unicorn_conf} -E #{app_env} -p #{unicorn_port} -D"
  end
  task :stop do
    run "if [ -f #{unicorn_pid} ]; then kill -QUIT `cat #{unicorn_pid}`; fi; true"
    run "rm #{unicorn_pid}; true"
  end
end

task :select_release do
  logger.info "These are the latest releases on git:"
  show_releases_list
  set :release_to_deploy, Capistrano::CLI.ui.ask("Enter git release to deploy (enter to deploy latest): ") {|q| q.default = last_release}
  set :branch,  "release/#{release_to_deploy}"
end

task :create_release do
  show_releases_list
  create_release
end

def releases_list
  return @releases_list if @releases_list
  @releases_list = `git branch -r`.split("\n")
  @releases_list = @releases_list.find_all{|t| t.include?('release')}.collect{|t| t.gsub(/\s+/, "").gsub('origin/release/','')}
end

def last_release
  @releases_list.first
end

def create_release
  position_in_month = (Date.today.day / 15) + 1
  release_name      = Capistrano::CLI.ui.ask("Enter git release name (ex.: #{Date.today.strftime("%y%m")}_0#{position_in_month}.01): ")
  `git branch release/#{release_name}`
  `git checkout release/#{release_name}`
  `git push origin release/#{release_name}`
end

def show_releases_list
  puts "releases on repository:"
  if releases_list.empty?
    logger.info " ===== Create release ====="
  else
    logger.info last_release + " <===== LATEST"
    if releases_list.size > 5
      1.upto(4) do |i|
        logger.info releases_list[i]
      end
      
    else
      1.upto(releases_list.size-1) do |i|
        logger.info releases_list[i]
      end
    end
  end
end
