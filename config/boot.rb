ENV["RACK_ENV"] ||= "development"

require 'bundler'
Bundler.setup

Bundler.require(:default, ENV["RACK_ENV"].to_sym)

require File.expand_path(File.join(File.dirname(__FILE__), "estabilish_connection"))

Dir["./models/**/*.rb"].each { |f| require f }
Dir["./lib/**/*.rb"].each { |f| require f }


if ENV["RACK_ENV"].include?('approval')
  
  scheduler = Rufus::Scheduler.start_new

  scheduler.cron '30 3 * * *' do
    DataUpdateStat.create_last
    CrawlerStatus.start_crawler
      begin
        load_products_from_compario = WorkitCrawler::LoadProductsFromCompario.new
        load_products_from_compario.create
      rescue
        ActiveRecord::Base.establish_connection(@@connection_info[ENV['RACK_ENV']])
        WorkitCrawler.puts_action('Failed to load products on compario in rake')
      end
      
      begin
        workit_load = WorkitCrawler::WorkitLoad.new
        workit_load.save_files_on_database
      rescue
        WorkitCrawler.puts_action('Failed to ftp file from ftp in rake')
      end

      begin
        migration_to_olp = WorkitCrawler::MigrationToOlp.new
        migration_to_olp.migrate_all
      rescue
        WorkitCrawler.puts_action('Failed to migrate the data to database in rake')
      end
    CrawlerStatus.end_crawler
    DataUpdateStat.end_last

    begin
      workit_sender = WorkitCrawler::WorkitSender.new
      workit_sender.generate_and_send
    rescue
      WorkitCrawler.puts_action('Failed send file to ftp in rake')
    end
  end

  scheduler.cron '00 8 * * *' do
    DataUpdateStat.create_last
    CrawlerStatus.start_crawler      
      begin
        workit_load = WorkitCrawler::WorkitLoad.new
        workit_load.save_files_on_database
      rescue
        WorkitCrawler.puts_action('Failed to ftp file from ftp in rake')
      end

      begin
        migration_to_olp = WorkitCrawler::MigrationToOlp.new
        migration_to_olp.migrate_all
      rescue
        WorkitCrawler.puts_action('Failed to migrate the data to database in rake')
      end
    CrawlerStatus.end_crawler
    DataUpdateStat.end_last
  end
end