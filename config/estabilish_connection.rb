@@connection_info = YAML::load_file(File.join(File.dirname(File.expand_path(__FILE__)), 'database.yml'))
ActiveRecord::Base.establish_connection(@@connection_info[ENV['RACK_ENV']])