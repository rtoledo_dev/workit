require 'factory_girl'
FactoryGirl.define do
  sequence :random_string do |n|
    "string#{n}--#{rand(n)}"
  end

  factory :daily_indicator do
    icrt '0001'
    country 'brazil'
    combine_indicator 250.3
    created_at '2001-02-03'
    qty_offline_prices 10
    minimum_offline_prices 299.9
    offline_indicator 399.9
    qty_found 40
    minimum_online_prices 599.9
    online_indicator 699.9
    value_daily_indicator 0
  end

  factory :daily_indicator_es, parent: :daily_indicator do
    icrt '0002'
    country 'spain'
  end

  factory :daily_indicator_fr, parent: :daily_indicator do
    icrt '0003'
    country 'belgium_fr'
  end

  factory :daily_indicator_nl, parent: :daily_indicator do
    icrt '0004'
    country 'belgium_nl'
  end

  factory :daily_indicator_pt, parent: :daily_indicator do
    icrt '0005'
    country 'portugal'
  end

  factory :daily_indicator_it, parent: :daily_indicator do
    icrt '0006'
    country 'italy'
  end

  factory :crawler_status do
    process_timestamp Date.today
    action 'Starting'
    status 'Wait'
  end

  factory :category do
    name {generate(:random_string)}
  end

  factory :site do
    name {generate(:random_string)}
    country 'brazil'
  end

  factory :site_category do
    site
    category
  end

  factory :all_product do
    country "brazil"
    name "Nice Input Base Product Name"
    price "R$ 1"
    site
    site_category
    unique_id { rand(99999999999999999999999)+1}
  end

  factory :all_product_price

  factory :base_product do
    brand {Forgery::Name.company_name}
    model {Forgery::Name.full_name}
    category
    
  end

  factory :product do
    country "brazil"
    name "Nice Input Base Product Name"
    price 1
    site
    unique_id {ActiveSupport::SecureRandom.hex(10)}

    # factory :product_with_url do
    #   url {Forgery::Internet.domain_name}
    # end

    # factory :product_for_matching do
    #   site_category
    #   unique_id 111
    # end

    # factory :product_with_matching_suggestion do
    #   after_create do |product|
    #     mm =FactoryGirl.create(:manual_matching, :manual_choice=>'yes', :product => product)
    #     product.manual_matchings << mm
    #   end
    # end

    # factory :product_with_default_matching_suggestion do
    #   after_create do |product|
    #     mm =FactoryGirl.create(:manual_matching, :manual_choice=>nil, :product => product)
    #     product.manual_matchings << mm
    #   end
    # end
    
    # factory :product_with_matching do
    #   site
    #   unique_id 123
    #   after_create do|product|
    #     base_product = FactoryGirl.create(:base_product)
    #     matching = FactoryGirl.create(:basic_product_base_product, :product=>product, :base_product=> base_product)
    #     product.basic_product_base_product << matching
    #   end
    # end
  end

  factory :basic_products_base_products do
    base_product
    product
  end





  # factory :workit_product_price do
  #   workit_bpl_list_id 1
  #   t.integer :base_product_id
  #   t.string :icrt_code
  #   t.string :site_name
  #   t.string :brand
  #   t.string :title
  #   t.float :price
  #   t.float :variation
  #   t.boolean :availability
  #   t.integer :delivery
  #   t.string :url
  #   t.string :image_url
  #   t.datetime :modified_at
  #   t.float :delivery_price
  #   t.string :status, :default => 'waiting-processing'
  #   t.text :import_errors
  # end

  factory :data_update_stat do
  end
end