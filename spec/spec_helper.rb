ENV['RACK_ENV'] = "test"
require "simplecov"
SimpleCov.start

require File.expand_path(File.join(File.dirname(__FILE__), "..", "config", "boot"))

require File.expand_path(File.join(File.dirname(__FILE__), "schema"))
require File.expand_path(File.join(File.dirname(__FILE__), "factories"))

Webrat.configure do |conf|
  conf.mode = :rack
end

RSpec.configure do |conf|
  conf.include Rack::Test::Methods
  conf.include Webrat::Methods
  conf.include Webrat::Matchers

  conf.before(:suite) do
    DatabaseCleaner.strategy = :transaction
  end
  conf.before(:each) do
    DatabaseCleaner.start
  end
  conf.after(:each) do
    DatabaseCleaner.clean
  end
end

# def load_tables_for_compario_test
#   ActiveRecord::Schema.define do
#     self.verbose = false

#     create_table :daily_indicators, :force => true do |t|
#       t.string :icrt
#       t.string :country
#       t.float :combine_indicator
#       t.integer :qty_offline_prices
#       t.float :minimum_offline_prices
#       t.float :offline_indicator

#       t.integer :qty_found
#       t.float :minimum_online_prices
#       t.float :online_indicator

#       t.string :warning_online_indicator

#       t.float :value_daily_indicator, default: 0
#       t.timestamps
#     end
#   end
# end