require "spec_helper"

describe Product do
  context "attributes to be removed" do
    it 'have removable attributes' do
      Product.new.attributes.should include('Numerical_price', 'currency')
    end

    it 'havent removable attributes' do
      product = Product.new
      product.remove_attributes
      product.attributes.should_not include('Numerical_price', 'currency')
    end
  end
end