require 'spec_helper'

describe Util do
  it "should return unique id to product" do
    site          = FactoryGirl.create(:site, :country=>'brazil')
    site_category = FactoryGirl.create(:site_category, :site=>site)
    url           = "www.americanas.com.br"
    Util.generate_unique_id(url, "samsung ace", site_category.id, site.id).should == "cde2cf7bcaf589392c6d55bb26fdb50e"
  end
  it "should return unique id to product" do
    site          = FactoryGirl.create(:site, :country=>'brazil')
    site_category = FactoryGirl.create(:site_category, :site=>site)
    url           = "http://www.elcorteingles.es/tienda/informatica/browse/subcategory.jsp;jsessionid=16A2D18F067F9A2930A02AF477A0D52A.eci_node10?categoryId=999.1255600829&navAction=push&navCount=0&addFacet=9004:999.1255600829"
    Util.generate_unique_id(url, "iphone 4S 32 GB", site_category.id, site.id).should =="813e31c5d2dafbad3b34560c90d4439f"
  end

  it "should return unique id to product" do
    site          = FactoryGirl.create(:site, :country=>'brazil')
    site_category = FactoryGirl.create(:site_category, :site=>site)
    url           = "http://www.alientech.pt/index.php?cPath=241?osCsid=b25f74a5fdf652e94e51fb3edf42af1d"
    Util.generate_unique_id(url, "ipad 3", site_category.id, site.id).should =="a0229666e61619be9320fa9d2cf147a3"
  end

  it "should return unique id to product" do
    site          = FactoryGirl.create(:site, :country => 'brazil')
    site_category = FactoryGirl.create(:site_category, :site => site)
    url           = "http://www.amazon.fr/gp/browse/ref=sr_tc_img_2_0?node=57850031&ie=UTF8&dp=1338582372#/ref=sr_pg_2?rh=n%3A908826031%2Cn%3A!908827031%2Cn%3A1387423031%2Cn%3A57850031&page=2&ie=UTF8&qid=1338582418"
    Util.generate_unique_id(url, "ipad 3", site_category.id, site.id).should =="eeca353fc9df240c6b13849222b57eae"
  end

  it "remove_tmp_params_from_url in ref and dp" do
    url = "http://www.amazon.fr/gp/browse/ref=sr_&/dp/=1338582372"
    Util.remove_tmp_params_from_url(url).should =="http://www.amazon.fr/gp/browse/"
  end

  it "remove_tmp_params_from_url rescue fail" do
    URI.stub(:parse).and_raise
    url = "http://www.amazon.fr/gp/browse/ref=sr_&/dp/=1338582372"
    Util.remove_tmp_params_from_url(url).should =="http://www.amazon.fr/gp/browse/ref=sr_&/dp/=1338582372"
  end

  it '.generate_base_product_unique_id' do
    result = Digest::MD5.hexdigest('ab cd')
    Util.generate_base_product_unique_id('ab','cd').should eql(result)
  end

  it '.generate_site_and_category_unique_id' do
    Digest::MD5.stub(:hexdigest).and_return('0647c6e97bb9d4b81bbc3f0fd3d5d50b')
    Util.generate_site_and_category_unique_id(1235,'test...').should eql('0647c6e9-7bb9-d4b8-1bbc-3f0fd3d5d50b')
  end
end