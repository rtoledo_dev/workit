require "spec_helper"

describe WorkitCrawler do
  it '.puts_action' do
    WorkitCrawler.should_receive('puts').with('---- testing').once
    WorkitCrawler.puts_action('testing')
  end
end