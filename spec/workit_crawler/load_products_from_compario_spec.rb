require "spec_helper"

describe WorkitCrawler::LoadProductsFromCompario do

  context "Class helper methods" do
    it '.clean_input_base_products' do
      InputBaseProduct.connection.should_receive(:execute).with('TRUNCATE TABLE input_base_products')
      WorkitCrawler::LoadProductsFromCompario.clean_input_base_products
    end

    it '.clean_null_input_base_products' do
      InputBaseProduct.connection.should_receive(:execute).with('DELETE FROM input_base_products WHERE brand IS NULL and model IS NULL')
      WorkitCrawler::LoadProductsFromCompario.clean_null_input_base_products
    end

    it '.database_definitions' do
      WorkitCrawler::LoadProductsFromCompario.database_definitions['test'].keys.should eql(["compario_BR_BR", "compario_BE_FR", "compario_BE_NL", "compario_IT_IT", "compario_ES_ES", "compario_PT_PT", 'compario_TEST'])
    end

    context 'establish connection'
    it '.compario_connection ok' do
      ActiveRecord::Base.stub(:establish_connection).and_return(true)
      expect(WorkitCrawler::LoadProductsFromCompario.compario_connection('BE_FR')).to be_true
    end

    it '.compario_connection not exists' do
      expect(WorkitCrawler::LoadProductsFromCompario.compario_connection('wrong')).to be_false
    end

    it '.compario_connection fail' do
      ActiveRecord::Base.stub(:establish_connection).and_raise('Invalid connection information')
      expect{ WorkitCrawler::LoadProductsFromCompario.compario_connection('BE_FR') }.to raise_error('Invalid connection information')
    end
  end

  context "Instance helper methods" do
    before(:each) do
      @load_products_from_compario = WorkitCrawler::LoadProductsFromCompario.new
    end


    it '#categories' do
      2.times do
        FactoryGirl.create(:category)
      end
      
      @load_products_from_compario.categories.should have(2).elements
    end

    it '#category_countries' do
      @load_products_from_compario.category_countries.should eql(['IT_IT'])
    end

    it '#categories_to_id' do
      FactoryGirl.create(:category, name: 'test category')
      @load_products_from_compario.categories_to_id.should eql("WHEN FamilyName = 'test category' then 1 WHEN FamilyName = 'Digital cameras' then 4")
    end

    it '#categories_names_to_sql_in' do
      FactoryGirl.create(:category, name: 'test category-1')
      FactoryGirl.create(:category, name: 'test category-2')
      expect(@load_products_from_compario.categories_names_to_sql_in).to eql("'test category-1','test category-2'")
    end

    it '#country_map' do
      @load_products_from_compario.country_map.should eql("CASE WHEN Language = 'BE_NL' THEN 'belgium_nl' WHEN Language = 'BE_FR' THEN 'belgium_fr' WHEN Language = 'BR_BR' THEN 'brazil' WHEN Language = 'ES_ES' THEN 'spain' WHEN Language = 'IT_IT' THEN 'italy' WHEN Language = 'IT' THEN 'italy' WHEN Language = 'PT_PT' THEN 'portugal' ELSE 'undefined' END")
    end

    it '#status_map' do
      @load_products_from_compario.status_map.should eql("CASE WHEN Status = '1' THEN 1 WHEN Status = 'True' THEN 1 ELSE 0 END")
    end

  end

  # context "connection on compario" do
  #   before(:each) do
  #     @load_products_from_compario = WorkitCrawler::LoadProductsFromCompario.new
  #   end

  # end
end