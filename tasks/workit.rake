namespace :workit do

  desc 'Build product families to countries'
  task :rebuild_product_families do
    require File.dirname(__FILE__) + '/../config/boot_without_webservice.rb'
    if ENV["RACK_ENV"] == 'test'
      require File.dirname(__FILE__) + '/../spec/schema.rb'
    end

    categories = ENV['CATEGORIES'].nil? ? nil : ENV['CATEGORIES'].split(',')
    countries  = ENV['COUNTRIES'].nil? ? nil : ENV['COUNTRIES'].split(',')
    WorkitProductFamily.rebuild_by_countries_and_categories(countries, categories)
  end

  desc 'Run all'
  task :run_all do
    require File.dirname(__FILE__) + '/../config/boot_without_webservice.rb'
    if ENV["RACK_ENV"] == 'test'
      require File.dirname(__FILE__) + '/../spec/schema.rb'
    end

    DataUpdateStat.create_last
    CrawlerStatus.start_crawler
      begin
        load_products_from_compario = WorkitCrawler::LoadProductsFromCompario.new
        load_products_from_compario.create
      rescue
        ActiveRecord::Base.establish_connection(@@connection_info[ENV['RACK_ENV']])
        WorkitCrawler.puts_action('Failed to load products on compario in rake')
      end
      
      begin
        workit_load = WorkitCrawler::WorkitLoad.new
        workit_load.save_files_on_database
      rescue
        WorkitCrawler.puts_action('Failed to ftp file from ftp in rake')
      end

      begin
        migration_to_olp = WorkitCrawler::MigrationToOlp.new
        migration_to_olp.migrate_all
      rescue
        WorkitCrawler.puts_action('Failed to migrate the data to database in rake')
      end
    CrawlerStatus.end_crawler
    DataUpdateStat.end_last

    begin
      workit_sender = WorkitCrawler::WorkitSender.new
      workit_sender.generate_and_send
    rescue
      WorkitCrawler.puts_action('Failed send file to ftp in rake')
    end
  end

  desc 'Load products from compario'
  task :load_products_from_compario do
    require File.dirname(__FILE__) + '/../config/boot_without_webservice.rb'
    if ENV["RACK_ENV"] == 'test'
      require File.dirname(__FILE__) + '/../spec/schema.rb'
    end

    load_products_from_compario = WorkitCrawler::LoadProductsFromCompario.new
    load_products_from_compario.create
  end

  desc 'Read FTP, download files into tmp folder and process it'
  task :load_and_save do
    require File.dirname(__FILE__) + '/../config/boot_without_webservice.rb'
    if ENV["RACK_ENV"] == 'test'
      require File.dirname(__FILE__) + '/../spec/schema.rb'
    end

    workit_load = WorkitCrawler::WorkitLoad.new
    workit_load.save_files_on_database
  end

  desc 'Populate crawler tables'
  task :migrate_to_database do
    require File.dirname(__FILE__) + '/../config/boot_without_webservice.rb'

    migration_to_olp = WorkitCrawler::MigrationToOlp.new
    migration_to_olp.migrate_all
  end

  desc 'Generate and send XML file to workit'
  task :generate_and_send do
    require File.dirname(__FILE__) + '/../config/boot_without_webservice.rb'

    workit_sender = WorkitCrawler::WorkitSender.new
    workit_sender.generate_and_send
  end
end