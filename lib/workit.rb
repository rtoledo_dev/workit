module Workit
  class Service < Sinatra::Base
    helpers Sinatra::JSON
    
    get '/products_by_country' do
      json WorkitBplList.by_country(params['country'])
    end
  end
end